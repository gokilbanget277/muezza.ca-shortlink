<?php
    function generate_random_letters($length) {
        $random = "";
        for ($i = 0; $i < $length; $i++) {
            $random .= chr(rand(ord("a"), ord("z")));
        }
        return $random;
    }

    // Full URL to link shorted with *NO* trailing slash
    $server = "https://muezza.ca/s";
    // Some older servers might require /index.php?id= but this shouldn't be an issue
    $short_link_base = $server."/?id=";
    $code_length = 7;

    $db = new SQLite3("linkShortener.sqlite");
    $db->query("create table if not exists ShortLinks (id INTEGER PRIMARY KEY, link TEXT NOT NULL, code TEXT NOT NULL);");
?>

<html>
<head>
    <title>Muezza.ca ShortLink</title>
</head>
<body>
<center>
   <pre>
    __  ___                                 ______            __  __   _      __
   /  |/  /_ _____ ______ ___ _ _______ _  / __/ /  ___  ____/ /_/ /  (_)__  / /__
  / /|_/ / // / -_)_ /_ // _ `// __/ _ `/ _\ \/ _ \/ _ \/ __/ __/ /__/ / _ \/  '_/
/_/  /_/\_,_/\__//__/__/\_,_(_)__/\_,_/ /___/_//_/\___/_/  \__/____/_/_//_/_/\_\
   </pre>
    <pre>
        _                ___       _.--.
        \`.|\..----...-'`   `-._.-'_.-'`
      /  ' `         ,       __.--'
)/' _/     \   `-_,   /
            `-'" `"\_  ,_.-;_.-\_ ',     fsc/as
      _.-'_./   {_.'   ; /
    {_.-``-'         {_/
   </pre>
    <?php
        if($_SERVER["REQUEST_METHOD"] === "POST"){
            if(isset($_POST["link"])){
                $url = $_POST["link"];

                if(filter_var($url, FILTER_VALIDATE_URL)){
                    $max_attempts = 6;
                    $current_attempt = 0;
                    $validated_unique = false;
                    $unique_code = generate_random_letters($code_length);

                    while($current_attempt <= $max_attempts){
                        $queryResult = $db->querySingle("select id from ShortLinks where code = '{$unique_code}';");

                        if(isset($queryResult)){
                            $unique_code = generate_random_letters($code_length);
                        } else {
                            $validated_unique = true;
                            break;
                        }

                        $current_attempt++;
                    }

                    if($validated_unique){
                        $safe_url = SQLite3::escapeString($url);
                        $short_url = $short_link_base.$unique_code;
                        $db->query("insert into ShortLinks (link, code) values ('{$safe_url}', '{$unique_code}');");

                        echo "<font color='red'>New short link is:</font> <a href='{$short_url}'>{$short_url}</a><br><br>
                              <a href='{$server}'>Go home.</a>";
                    } else {
                        echo "<font color='red'>We couldn't generate a unique short link. Try Again. :(</a><br><br>
                              <a href='{$server}'>Go home.</a>";
                    }
                } else {
                    echo "<font color='red'><b>Invalid URL - Try Again</b></font>
                    <br><br>You must include <i>http://</i> in URLs.
                    <br><a href='http://www.faqs.org/rfcs/rfc2396.html'>RFC 2396</a><br><br>
                    <a href='{$server}'>Go home.</a>";
                }
            } else {
                echo "<font color='red'><b>URL wasn't provided in POST request.</b></font><br><br>
                    <a href='{$server}'>Go home.</a>";
            }
        } else {
            if(isset($_GET["id"])){
                $code = SQLite3::escapeString($_GET["id"]);
                $queryResult = $db->querySingle("select link from ShortLinks where code = '{$code}';");

                if(isset($queryResult)){
                    echo "<meta http-equiv='refresh' content='3; url={$queryResult}' />
                     Attempting to redirect you in 3 seconds...<br><br>
                     Click the following link if redirect fails:<br>
                     <a href='{$queryResult}'>{$queryResult}</a>";
                } else {
                    echo "Code <i>{$code}</i> wasn't found.<br><br>
                     <a href='{$server}'>Go home.</a>";
                }

            } else {
                echo "<form action='' method='post'>
                  Link:
                  <input type='text' name='link' />
                  <br><br>
                  <button type='submit'>Shorten</button> 
                 </form>";
            }
        }
    ?>
</center>
</body>
</html>