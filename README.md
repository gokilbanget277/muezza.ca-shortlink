# Muezza.ca Shortlink

This is a very simple shortlink generator/redirect script using PHP and SQlite.

This script is self contained, it will create the DB it needs the first time it's loaded
or if it's unable to find an existing DB.

This script/page uses no CSS, no JS, and is basicaly as bare bones as you can get. It
will probably work in every web browser under the sun.

SQL is properly escaped and the script runs fast, but obviously isn't for heavy use, just personal.

It would be easy enough to convert it to use MySQL, but I didn't want to have to run a full database.

I have provided a `.htaccess` file to block access to the DB file. If you don't upload this
or if you change the DB name and don't update it anyone will be able to download the DB.